<?php

require_once __DIR__ . '/lib/classes/DB.php';
require_once __DIR__ . '/lib/classes/View.php';
require_once __DIR__ . '/lib/models/BaseModel.php';
require_once __DIR__ . '/lib/models/News.php';

$template = __DIR__ . '/lib/templates/newsArticle.php';

$news = new News;
$view = new View;

// заполняем данные и выводим шаблон
if (isset($_GET['id'])) {

    $article = $news->getArticleById($_GET['id']);

    // проверяем наличие объекта с новостью
    if ($article instanceof Article) {
        $view
            ->assign('article', $article)
            ->display($template);
    } else {
        header('HTTP/1.0 404 Not Found');
        die();
    }

} else {
    header('Location: /');
    die();
}
