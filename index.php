<?php

require_once __DIR__ . '/lib/classes/DB.php';
require_once __DIR__ . '/lib/classes/View.php';
require_once __DIR__ . '/lib/models/BaseModel.php';
require_once __DIR__ . '/lib/models/News.php';

$template = __DIR__ . '/lib/templates/newsMain.php';

$news = new News;
$view = new View;

$view
    ->assign('records', $news->getAllNews())
    ->display($template);