<?php

/*
 * Создайте класс DB
 * В конструкторе устанавливается и сохраняется соединение с базой данных. Параметры соединения берем из файла конфига
 * Метод execute(string $sql) выполняет запрос и возвращает true либо false в зависимости от того, удалось ли выполнение
 * Метод query(string $sql, array $data) выполняет запрос, подставляет в него данные $data, 
 * возвращает данные результата запроса либо false, если выполнение не удалось
*/

class DB
{
    protected $dbh = null;

    public function __construct()
    {
        $config = include(__DIR__ . '/../config.php');
        $dsn = $config['driver'] . ':host=' . $config['host'] . ';dbname=' . $config['dbname'];
        $this->dbh = new PDO($dsn, $config['username'], $config['pass']);
    }

    public function execute(string $sql)
    {
        $sth = $this->dbh->prepare($sql);
        return $sth->execute();
    }

    public function query(string $sql, array $data)
    {
        // готовим запрос
        $sth = $this->dbh->prepare($sql);

        // запускаем запрос, execute возвращает bool начение
        if (true === $sth->execute($data)) {
            // если всё успешно выполнилось, возвращаем результат
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } else {
            return false;
        }
    }

}