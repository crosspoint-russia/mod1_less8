<?php

class Article
{
    protected $title;
    protected $story;
    protected $author;

    public function __construct($title, $story, $author)
    {
        $this->title = $title;
        $this->story = $story;
        $this->author = $author;
    }

    // Серия геттеров
    
    public function getTitle()
    {
        return $this->title;
    }

    public function getStory()
    {
        return $this->story;
    }

    public function getAuthor()
    {
        return $this->author;
    }
    
}