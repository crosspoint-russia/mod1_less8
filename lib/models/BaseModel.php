<?php

class BaseModel
{
    // защищенное свойство объекта для хранения данных
    protected $table;
    protected $db;

    // определяем таблицу для работы, создаем и сохраняем в себе экземпляр DB
    public function __construct()
    {
        $this->table = strtolower(get_class($this));
        $this->db = new DB;
    }

    // получение всех записей из таблицы
    public function getAllRecords($orderDirection = 'DESC', $orderBy = 'id')
    {
        return $this->db->query('SELECT * FROM ' . $this->table . ' ORDER BY ' . $orderBy . ' ' . $orderDirection, []);
    }

    // получение записи по id
    public function getRecordById(int $id)
    {
        $tempArray = [];
        $tempArray[':id'] = $id;
        return $this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=:id', $tempArray);
    }

/* 
* WARNING
* код ниже НЕ по ТЗ, выполнен в рамках исследования и его можно НЕ читать
*/
    protected function prepareArray($data)
    {
        $queryData = [];
        foreach ($data as $key => $value) {
            $queryData[':' . $key] = $value;
        }
        return $queryData;
    }

    public function updateRecordById(int $id, array $data)
    {
        $queryString = 'UPDATE ' . $this->table . ' SET ';

        $tempArray = [];
        foreach ($data as $key => $value) {
            $tempArray[] = $key . '=:' . $key;
        }
        $queryString .= implode(', ', $tempArray);

        $queryString .= ' WHERE id=:id';

        $queryData = $this->prepareArray($data);
        $queryData[':id'] = $id;

        return $this->db->query($queryString, $queryData);
    }

    public function insertRecord(array $data)
    {
        $queryString = 'INSERT INTO ' . $this->table . ' (';
        $queryString .= implode(', ', array_keys($data)) . ') VALUES (';

        $tempArray = [];
        foreach ($data as $key => $value) {
            $tempArray[] = ':' . $key;
        }
        $queryString .= implode(', ', $tempArray);

        $queryString .= ')';
        $queryData = $this->prepareArray($data);

        return $this->db->query($queryString, $queryData);
    }

    public function deleteRecordById($id)
    {
        $tempArray = [];
        $tempArray[':id'] = $id;
        return $this->db->query('DELETE FROM ' . $this->table . ' WHERE id=:id', $tempArray);
    }


}