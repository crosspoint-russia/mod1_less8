<?php

require_once __DIR__ . '/Article.php';

// наследуемся от базовой модели
class News extends BaseModel
{

    // метод получения всех новостей
    public function getAllNews()
    {
        $queryResult = $this->getAllRecords();
        $articles = [];

        // если метод базовой модели getAllRecords не вернул false
        if (false !== $queryResult) {
            foreach ($queryResult as $record) {
                $articles[$record['id']] = new Article($record['title'], $record['text'], $record['author']);
            }
        }

        return $articles;
    }

    // метод получения одной новости
    public function getArticleById($id)
    {
        $queryResult = $this->getRecordById($id);
        $article = null;

        // если метод базовой модели getRecordById не вернул false
        if (false !== $queryResult) {
            if (!empty($queryResult)) {
                $article = new Article($queryResult[0]['title'], $queryResult[0]['text'], $queryResult[0]['author']);
            }
        }
        return $article;
    }

}