<?php
$article = $this->data['article'];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $article->getTitle(); ?></title>
</head>
<body>

<h1><?php echo $article->getTitle(); ?></h1>

<p>
    <?php echo $article->getStory(); ?>
</p>
<p>
    <?php echo $article->getAuthor(); ?>
</p>

<a href="/">Return to all news</a>

</body>
</html>