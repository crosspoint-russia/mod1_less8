<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Новости</title>
</head>
<body>

<h1>Новости</h1>

<ul>
    <?php foreach ($this->data['records'] as $key => $article) : ?>
    <li>
        <a href="/article.php?id=<?php echo $key; ?>"><?php echo $article->getTitle(); ?></a>
        <p>
            <?php echo $article->getStory(); ?>
        </p>
    </li>
    <?php endforeach; ?>
</ul>

</body>
</html>